docker-compose up -d
docker-compose exec images pipenv run python3 manage.py migrate
docker-compose exec images pipenv run python3 manage.py loaddata ./thumbnails/fixtures/basic_plans.json
docker-compose exec images pipenv run python3 manage.py createsuperuser
