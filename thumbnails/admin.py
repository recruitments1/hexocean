# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import Plan, ThumbnailDetails, Profile, Product


class ThumbnailDetailsInline(admin.TabularInline):
    model = ThumbnailDetails


class PlanAdmin(admin.ModelAdmin):
    model = Plan
    inlines = [ThumbnailDetailsInline, ]
    list_display = ['name', 'is_original_link', 'can_generate_link']


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profiles'
    list_display = ['plan']


class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInline,)
    list_display = ['username', 'email', 'first_name', 'last_name', 'get_plan', 'is_staff']

    def get_plan(self, instance: User) -> Plan:
        return instance.profile.plan

    get_plan.short_description = 'Plan'


admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(Plan, PlanAdmin)
admin.site.register(Product)
admin.site.register(User, UserAdmin)
