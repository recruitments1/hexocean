FROM python:3.8

WORKDIR /home/app
RUN pip install pipenv
COPY . .
RUN pipenv install --skip-lock

CMD ["pipenv", "run", "python", "manage.py", "runserver", "0.0.0.0:8000"]
