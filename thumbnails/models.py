# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext as _
from versatileimagefield.fields import VersatileImageField


class Plan(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    is_original_link = models.BooleanField(
        _("Is original link"), default=False)
    can_generate_link = models.BooleanField(
        _("Can generate link"), default=False)

    def __str__(self) -> str:
        return self.name


class ThumbnailDetails(models.Model):
    plan = models.ForeignKey(
        Plan, on_delete=models.CASCADE, related_name='thumbnails_details')
    height = models.IntegerField(_("Height"))

    def __str__(self) -> str:
        return f'{self.plan.name} thumbnail: {self.pk}'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    plan = models.ForeignKey(Plan, on_delete=models.SET_NULL, null=True)

    def __str__(self) -> str:
        return f'{self.user.username}'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs) -> None:
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs) -> None:
    instance.profile.save()


class Product(models.Model):
    user = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name='products')


class Image(models.Model):
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name='images')
    image = VersatileImageField(
        'Image',
        upload_to='images/',
    )


class TemporaryLink(models.Model):
    user = models.ForeignKey(Profile, verbose_name=_(
        "User"), on_delete=models.CASCADE)
    duration_time = models.IntegerField(_('Duration time'))
    creation_date = models.DateTimeField(
        _('Creation date'), default=datetime.now())
    image = models.ImageField(_("Image"))

    def is_active(self) -> bool:
        return datetime.timestamp(self.creation_date) + self.duration_time > datetime.timestamp(datetime.now())
