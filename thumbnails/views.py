# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from typing import List
from django.urls import reverse
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404
from rest_framework import status
from django.http import FileResponse
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from versatileimagefield.fields import VersatileImageField

from .models import Image, Product, Profile, TemporaryLink
from .serializers import (CreateProductImageSerializer, ImageSerializer,
                          ListProductSerializer)


class TemporaryLinksViewSet(APIView):
    def post(self, request: Request, *args, **kwargs) -> Response:
        image = Image.objects.get(pk=self.kwargs['pk'])
        profile = self.get_profile(request)
        if not profile.plan.can_generate_link:
            return Response(status=status.HTTP_403_FORBIDDEN)
        if image.product.user.pk != profile.pk:
            return Response(status=status.HTTP_403_FORBIDDEN)
        try:
            duration_time = int(request.data.get('duration_time'))
        except TypeError:
            duration_time = None

        if not duration_time:
            return Response(data='No duration time.', status=status.HTTP_400_BAD_REQUEST)
        if duration_time < 300 or duration_time > 30_000:
            return Response(data='Duration has to be > 300 and < 30 000')
        temporary_link = TemporaryLink.objects.create(
            user=profile,
            duration_time=request.data['duration_time'],
            image=image.image
        )
        link = reverse('temporary-link', kwargs={'pk': temporary_link.pk})
        return Response(data={'link': link})

    def get(self, request, *args, **kwargs) -> Response:
        link = get_object_or_404(TemporaryLink, pk=self.kwargs['pk'])
        if link.is_active():
            return FileResponse(open(link.image.file.name, 'rb'))
        link.delete()
        return Response(status=status.HTTP_404_NOT_FOUND)

    def get_profile(self, request: Request) -> Profile:
        return get_object_or_404(Profile, user=request.user)


class ProductViewSet(ModelViewSet):
    def get_profile(self, request: Request) -> Profile:
        return get_object_or_404(Profile, user=request.user)

    def get_serializer_class(self):
        if self.action == 'create':
            return CreateProductImageSerializer
        return ListProductSerializer

    def get_queryset(self) -> QuerySet:
        return self.get_profile(self.request).products

    def create(self, request: Request) -> Response:
        profile = self.get_profile(request)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        product = Product.objects.create(user=self.get_profile(request))

        if not profile.plan:
            return Response('User has no plan assigned.', status=status.HTTP_403_FORBIDDEN)

        created_instance = serializer.save(product=product)
        image = created_instance.image
        created_images = self.create_thumbnails(image, profile, product)

        if not profile.plan.is_original_link:
            created_instance.image.delete()
            created_instance.delete()
        else:
            created_images.append(created_instance)

        headers = self.get_success_headers(serializer.data)
        return Response(ImageSerializer(created_images, many=True).data, status=status.HTTP_201_CREATED, headers=headers)

    def create_thumbnails(self,
                          original_image: VersatileImageField,
                          profile: Profile,
                          product: Product) -> List[Image]:
        thumbnails = []
        for thumbnail_details in profile.plan.thumbnails_details.all():
            ratio = original_image.width / original_image.height
            height = int(thumbnail_details.height)
            width = int(original_image.width / (original_image.height /
                        thumbnail_details.height) * ratio)
            thumbnail = original_image.thumbnail[f'{width}x{height}']
            new_thumbnail = Image.objects.create(
                product=product,
                image=thumbnail.name,
            )
            thumbnails.append(new_thumbnail)

        return thumbnails
