from rest_framework.routers import DefaultRouter

from .views import ProductViewSet, TemporaryLinksViewSet
from django.urls import path, include


router = DefaultRouter()
router.register(r'products', ProductViewSet, basename='product')


urlpatterns = [
    path('', include(router.urls)),
    path('links/<pk>/', TemporaryLinksViewSet.as_view(), name='temporary-link')
]
