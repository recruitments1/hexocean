# Thumbnails

## Notes

- Products composes images
- Images links can be retrieved by anyone (from /media)

## Run

- Install requirements
```
pipenv install
```

- Activate virtualenv
```
pipenv shell
```

- Export SECRET_KEY environmental variable
```
export SECRET_KEY=secret_key_value
```

- Run migrations

```
python3 manage.py migrate
```

- Load initial data

```
python3 manage.py loaddata ./thumbnails/fixtures/basic_plans.json
```

## Run with docker-compose

- Run application in docker
```
docker-compose up -d
```

- Run migrations
```
docker-compose exec images pipenv run python3 manage.py migrate
```

- Load initial data
```
docker-compose exec images pipenv run python3 manage.py loaddata ./thumbnails/fixtures/basic_plans.json
```

- Create superuser

```
docker-compose exec images pipenv run python3 manage.py createsuperuser
```

### Shortcut

```
chmod +x ./run-compose.sh
./run-compose.sh
```

## Time spent

About 5-6 hours.
